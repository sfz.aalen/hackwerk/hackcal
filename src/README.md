# Hackcal

This script is using [PHP ICS Parser](https://github.com/u01jmg3/ics-parser) to parse ics files, e.g. from Nextcloud. The output is an very simple json for embedding the calender into html websites. Its presorted by days and preformatted to use less js on client side for date interpretation.

---

## Installation

Just copy the files into your php capable webserver directory. Be sure, that the cache folder is writeable by the script and the locale set in the index.php is available on the system.

## Usage

Just call the url. You can filter by categories by adding /?filter=category to the request.

## Example integration using jQuery.

You will need to include a ``<div id="hackcal"></div>`` placeholder in your html page and embed also the jQuery js library.

```
(function($) {

    var cal_uri = "https://YOURDOMAIN/hackcal/";
    var uri_regex = /(https?:\/\/([-\w\.]+)+(:\d+)?(\/([\w\/\-_\.]*(\?\S+)?)?)?)/ig

    $.getJSON(cal_uri, function(data) {
	var items = [];
        $.each(data, function(date, day) {
            items.push("<tr data-date='" + date + "'><th colspan='3'>" + day.name + "</th><th>" + day.weekday + "</th></tr>");
            $.each(day.events, function(uid, event) {
		event.description = event.description.replace(/\n/g,"<br>").replace(uri_regex, "<a href='$1' target='_blank'>$1</a>");
                items.push("<tr data-uid='" + uid + "'><td>" + event.datestr + "</td><td>" + event.summary + "</td><td>" + ((event.location)?event.location:'') + "</td><td>" + ((event.description)?event.description:'') + "</td></tr>");
            });
        });
	$('#hackcal').html("<table><tbody>" + items.join( "" ) + "</tbody></table>");
    });

})(jQuery);
```

