<?php

# config
$ical = $_ENV["ICAL_URL"];
$cachefile = $_ENV["CACHE_FILE"];
$cachetime = $_ENV["CACHE_TIME"]; // 2 min

# requirements
require_once 'ICal/ICal.php';
require_once 'ICal/Event.php';

# Init Calendar
$iCal = new \ICal\ICal();

# Caching and load calendar
if(@filemtime($cachefile) + $cachetime < time()) {
  $ical_str = file_get_contents($ical);
  file_put_contents($cachefile, $ical_str);
  $iCal->initString($ical_str);
} else {
  $iCal->initFile($cachefile);
}
//$iCal->initURL($ical);

# Load calendar entries
$events = $iCal->sortEventsWithOrder($iCal->eventsFromInterval('1 month'));

$filter = filter_input(INPUT_GET, 'filter', FILTER_SANITIZE_SPECIAL_CHARS);

$result = [];
foreach ($events as $event) {

	if ($filter && strpos($event->categories, $filter) === false) {
		continue;
	}

	if (isset($event->categories) && strpos($event->categories, "hidden")) {
		continue;
	}

	$start = new DateTime($event->dtstart);
	$end = new DateTime($event->dtend);
	$uid = $event->uid;

	$interval = DateInterval::createFromDateString('1 day');
	$period = new DatePeriod($start, $interval, $end);

	foreach ($period as $dt) {
		$date = $dt->format("Y-m-d");

		$dateFormatter = new IntlDateFormatter('de_DE', IntlDateFormatter::LONG, IntlDateFormatter::NONE, 'Europe/Berlin', IntlDateFormatter::GREGORIAN);
		$weekdayFormatter = new IntlDateFormatter('de_DE', IntlDateFormatter::NONE, IntlDateFormatter::NONE, 'Europe/Berlin', IntlDateFormatter::GREGORIAN, 'EEEE');
		
		$result[$date]["name"] = $dateFormatter->format($dt->getTimestamp());
		$result[$date]["weekday"] = $weekdayFormatter->format($dt->getTimestamp());
		$result[$date]["events"][$uid]["dtstart"] = $event->dtstart; #TODO: Zeitzone aus lib auslesen
		$result[$date]["events"][$uid]["dtend"] = $event->dtend; #TODO: Zeitzone aus lib auslesen
		$result[$date]["events"][$uid]["datestr"] = (isset($event->dtstart_array[0]["VALUE"]) && $event->dtstart_array[0]["VALUE"] == 'DATE')?'Ganztägig':$start->format('H:i');
		$result[$date]["events"][$uid]["summary"] = $event->summary;
		$result[$date]["events"][$uid]["location"] = $event->location;
		$result[$date]["events"][$uid]["description"] = $event->description;
		if(isset($event->categories)) $result[$date]["events"][$uid]["categories"] = $event->categories;
	}

}

# Allow every page to load this json
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");

echo json_encode($result, JSON_PRETTY_PRINT);